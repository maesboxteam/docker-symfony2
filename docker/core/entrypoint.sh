#!/bin/bash
set -e

for SEDFILE in "$(ls /home/docker/src)"
do
	if [ -f $SEDFILE ]
	then
		echo $SEDFILE
	fi
done


if [ -d "/home/docker/app/cache" ]
then
	if [ -n "$(ls /home/docker/app/cache)" ]
	then
		rm -rf /home/docker/app/cache/*
		chmod 777 -R /home/docker/app/cache
		echo "cache deleted"
	fi
fi

if [[ -d "/home/docker/app/logs" ]]
then
	chmod 777 -R /home/docker/app/logs
	echo "logs dir writable"
fi


case $1 in 
	"apache"):
		shift
		exec apache2ctl $@
		;;
	"symfony"):
		shift
		exec php /home/docker/app/console $@
		;;
	"composer"):
		cd /home/docker
		exec $@
		;;
	*):
		exec "$@"
		;;
esac		
